# Flood\Canal: Admin

The user interface for [Canal Flat-File CMS](https://bitbucket.org/bemit_eu/canal-structure), see [Canal Bundle](https://bitbucket.org/bemit_eu/canal-bundle) for development of admin and backend.

- [Config](#markdown-header-config)
- [Setup](#markdown-header-setup)
- [Docker Image](#markdown-header-docker-image)
- [Download Build](#markdown-header-download-build)

## Config

See `.env` for the api endpoints against which the app runs, `env` variables could be used when building the admin..

If you want to change the config after the build, don't use `env` but change the `config.js` within the builded files.

## Setup

```bash
git clone --recurse-submodules -j8 https://bitbucket.org/bemit_eu/canal-admin.git

cd canal-admin

# install deps
npm i
# and again, as submodules depth is too much for npm atm.
npm i

# start dev-server & file watcher
npm start
# localhost:25021

# create production build
npm run build
# see ./build folder, copy files into webspace
# for usage with Apache: copy files within server_apache into build
# for usage with NodeJS: copy files within server_nodejs into webspace 
```

## Docker Image

[Docker Hub](https://hub.docker.com/r/bemiteu/canal-admin)

Creating a DockerImage with `server_nodejs` and `build`:

```bash
# Build Docker Local
# setup repo & install dependencies, then:
npm run build
cp ./server_nodejs/* ./build/

docker build -t <some-name> .

# run docker, `--rm` remove container after pressing e.g. `CTRL + C`, publish the port `25021` 
docker run --rm -p 25021:25021 <some-name>
# on windows use absolute path e.g. C:\Users will be //c/Users
docker run --rm -p 25021:25021 -v $(pwd)/config.js:/home/node/app/config.js <some-name>
```

### Pre-Build Images

```bash
# Releases
docker run -p 25021:25021 bemiteu/canal-admin:v0.2.1

# Branches
docker run -p 25021:25021 bemiteu/canal-admin:master
docker run -p 25021:25021 bemiteu/canal-admin:master_<build>
docker run -p 25021:25021 bemiteu/canal-admin:develop
docker run -p 25021:25021 bemiteu/canal-admin:develop_<build>
```

## Download Build

- for Apache
    - releases:
        - 0.2.1, [canal-admin_v0.2.1-apache.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/canal-admin_v0.2.1-apache.zip)
    - latest [master_apache.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/master_apache.zip)
    - latest [develop_apache.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/develop_apache.zip)
- for NodeJS
    - releases:
        - 0.2.1, [canal-admin_v0.2.1-nodejs.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/canal-admin_v0.2.1-nodejs.zip)
    - latest [master_nodejs.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/master_nodejs.zip)
    - latest [develop_nodejs.zip](https://bitbucket.org/bemit_eu/canal-admin/downloads/develop_nodejs.zip)

   
# Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

2018 - 2019 | [bemit UG](https://bemit.eu) (haftungsbeschränkt) - project@bemit.codes

Maintained by [Michael Becker](https://mlbr.xyz)