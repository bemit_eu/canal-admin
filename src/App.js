import React from 'react';
import Loadable from 'react-loadable';
import Loading from '@bemit/flood-admin/component/Loading';

import BaseApp from '@bemit/flood-admin/BaseApp';

import {addAlias, enableCustomDefault} from '@bemit/flood-admin/lib/i18n';

import {ReactComponent as Logo} from '@bemit/flood-admin/asset/logo.svg';
import {ReactComponent as LogoWhite} from '@bemit/flood-admin/asset/logo_white.svg';

enableCustomDefault('schema', 'article-meta');
enableCustomDefault('schema', 'doc-tree.block._content-common');
enableCustomDefault('schema', 'product.simple');
enableCustomDefault('schema', 'product.wine-product');
enableCustomDefault('doc-tree', 'selection');
enableCustomDefault('doc-tree', 'root');

addAlias('schema', 'doc-tree.block.banner', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.btn-block', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.event', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.gallery', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.headline-bold', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.image', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.text', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.text-image', 'doc-tree.block._content-common');

const features = {
    message: false,
    content: {
        'create-article': false
    },
    media: true,
    shop: false,
    marketing: false,
    user: false,
    analyze: false,
    block: false,
    connection: false,
    backendConnect: false,
    authPermission: false,
};

let config = {};
if(window.REACT_CONFIG) {
    config = window.REACT_CONFIG;
} else {
    console.error('App started without config, exit.');
    throw new Error('App started without config, exit.');
}

const api = {
    api: config.API_DEFAULT,
    file: config.API_DEFAULT,
    auth: config.API_AUTH
};

const routesComponents = {
    pages: {
        info: Loadable({
            loader: () => import('./page/Info'),
            loading: (props) => <Loading {...props} name='page/Info'/>,
        })
    },
    controlPanel: {
        dashboard: Loadable({
            loader: () => import('./page/ControlPanel/Dashboard'),
            loading: (props) => <Loading {...props} name='ControlPanel/Dashboard'/>,
        })
    }
};

const branding = {
    logo: {
        normal: Logo,
        white: LogoWhite,
    }
};

const locales = {
    en: {
        'common': {
            header: {
                slogan: 'Flood\\Canal'
            }
        },
        'page--auth': {
            title: 'Canal Editor'
        },
        'page--info': {
            title: 'Canal: Editor',
            info: {
                contact: {
                    support: {
                        subject: "Request Technical Solutions [Canal Editor]"
                    },
                    guide: {
                        subject: "Request User Support [Canal Editor]"
                    },
                    law: {
                        subject: "Company Request [Canal Editor]"
                    }
                }
            }
        }
    },
    de: {
        'common': {
            header: {
                slogan: 'Flood\\Canal'
            }
        },
        'page--auth': {
            title: 'Canal Editor'
        },
        'page--info': {
            title: 'Canal: Editor',
            info: {
                contact: {
                    support: {
                        subject: "Anfrage Technische Lösung [Canal Editor]"
                    },
                    guide: {
                        subject: "Anfrage Bedienungs Hilfe [Canal Editor]"
                    },
                    law: {
                        subject: "Unternehmens Anfrage [Canal Editor]"
                    }
                }
            }
        }
    }
};

class App extends React.Component {

    render() {

        return (
            <BaseApp api={api}
                     routesComponents={routesComponents}
                     locales={locales}
                     branding={branding}
                     features={features}
            />
        );
    }
}

export default App;
