import React from 'react';
import {withNamespaces} from '@bemit/flood-admin/lib/i18n';

class Dashboard extends React.Component {
    render() {
        const {t} = this.props;

        return (
            <React.Fragment>
                <h1>{t('title')}</h1>
            </React.Fragment>
        );
    }
}

export default withNamespaces('page--dashboard')(Dashboard);
