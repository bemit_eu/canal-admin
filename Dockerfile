
FROM node:12.2-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY ./build/ ./
COPY ./server_nodejs/ ./

USER node

RUN npm install --only=prod

EXPOSE 25021

CMD [ "node", "server.js" ]